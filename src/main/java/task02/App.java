package task02;
import  java.util.Scanner;

public class App {

    public static void main(String[] args) {
        final int attempts = 100;
        Scanner sc = new Scanner(System.in);
        System.out.println("Enter how many people should visit the party(more than 2): ");
        int N = sc.nextInt();
        int timesFullSpreaded = 0;
        int reached = 0;
        for(int i=0;i<attempts;i++){
            boolean guests[] = new boolean[N];
            guests[1] = true;
            boolean heard = false;
            int next = -1;
            int current = 1;
            while(!heard){
                next = 1 + (int)(Math.random() * (N-1));
                if(next == current){
                    while(next == current)
                        next = 1 + (int)(Math.random() * (N-1));
                }
                if(guests[next])
                {
                    if(spreaded(guests))
                        timesFullSpreaded++;
                    reached = reached + reached(guests);
                    heard = true;
                }
                guests[next] = true;
                current = next;
            }
        }
        System.out.println("Probability that everyone will hear rumor except Alice : " +
                (double)timesFullSpreaded/attempts);
        System.out.println("Average amount of people that rumor reached is: "+reached/attempts);
    }

    public static int reached(boolean array[]){
        int counter = 0;
        for(int i = 1;i<array.length;i++)
            if(array[i])
                counter++;
        return counter;
    }

    public static boolean spreaded(boolean array[]){
        for(int index = 1;index<array.length;index++)
            if(!array[index])
                return false;
        return true;
    }


}



